﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using AATKit_Example.Resources;
using AATKitWindows;
using System.Diagnostics;

namespace AATKit_Example
{
    public partial class MainPage : PhoneApplicationPage
    {
        public MainPage()
        {
            InitializeComponent();

            var bannerView = AATKit.GetPlacementView(App.BannerPlacementId);
            BannerPlace.Content = bannerView;

            AATKit.AATKitHaveAd += OnHaveAd;
            AATKit.AATKitNoAd += OnNoAd;
            AATKit.AATKitPauseForAd += OnPauseForAd;
            AATKit.AATKitResumeAfterAd += OnResumeAfterAd;
        }

        private void ShowFullscreenButtonTap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            AATKit.ShowPlacement(App.FullscreenPlacementId);
        }

        private void OnResumeAfterAd(int placementId)
        {
            Debug.WriteLine("AATKit: Resume after ad, placement: " + placementId);
        }

        private void OnPauseForAd(int placementId)
        {
            Debug.WriteLine("AATKit: Pause for ad, placement: " + placementId);
        }

        private void OnNoAd(int placementId)
        {
            Debug.WriteLine("AATKit: No ad for placement: " + placementId);
        }

        private void OnHaveAd(int placementId)
        {
            Debug.WriteLine("AATKit: Ad loaded for placement: " + placementId);
        }

    }
}